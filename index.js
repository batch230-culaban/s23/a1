/*
 Activity Instructions:

1. In the S23 folder, create an a1 folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
a. Name (String)
b. Age (Number)
c. Pokemon (Array)
d. Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a git repository named S23.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.

*/

let mainTrainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"],
    },
    talk: function(){
        console.log("Result of talk method");
        console.log("Pikachu! I choose you!");
    }
}

console.log(mainTrainer);

console.log("Result of dot notation:");
console.log(mainTrainer.name);
console.log("Result of square bracket notation:");
console.log(mainTrainer["pokemon"]);
mainTrainer.talk();


function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = level*2;
    this.attack = level;


    this.tackle = function(target){
        
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        

        if(target.health <= 0){
            console.log(target.name +"'s health is now reduced to "+ target.health);
            target.faint();
            console.log(target); 
        }else{
            console.log(target.name +"'s health is now reduced to "+ target.health);
            console.log(target);
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted");
    }
}


let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);